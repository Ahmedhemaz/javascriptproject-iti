/*******LocalStorageVariables********/
var sum = 0;
var allProductsCount = 0;
var cartArray = [];
// check if cartArray in localStorage and if yes will copy data to cartArray 
// to handle on pagereload
if (localStorage.getItem('cartArray')) {
    var cartItems = localStorage.getItem('cartArray');
    cartItems = JSON.parse(cartItems);
    cartArray = cartItems.slice();
}
var cartArrayString;

// fetch data asynchronously and alert in case of errors
// return promise of fetched data 
async function getData() {
    try {
        const result = await fetch(`https://gist.githubusercontent.com/a7med-hussien/7fc3e1cba6abf92460d69c0437ce8460/raw/da46abcedf99a3d2bef93a322641926ff60db3c3/products.json`);
        const data = await result.json();
        const Products = data.ProductCollection;
        return Products;
    } catch (error) {
        alert("reconnect to the Internet and retry later");
    }
};
// create html card for each product has been fetched 
// then return products to use it again
getData().then(products => {
    var cardDeckDiv = document.querySelector('.card-deck');
    products.forEach(product => {
        var cardDiv = document.createElement('div'); // create div for card
        var cardBody = document.createElement('div'); // create div for cardBody data
        var cardComponentsDiv1 = document.createElement('div'); // create div for firstBodyData of card
        var cardComponentsDiv2 = document.createElement('div'); // create div for secondBodyData of card
        var img = document.createElement('img'); // create img tag
        var H5ProductName = document.createElement('p'); // create p tag for productName
        var pProductPrice = document.createElement('p'); // create p tag for productPrice
        var cartI = document.createElement('i'); // create i for cartImg from fontAwesome
        var elUrl = document.createElement('a'); // create a tag to click on card Img
        img.className = 'card-img-top';
        H5ProductName.className = 'card-title';
        pProductPrice.className = 'product-price';
        cartI.className = 'btn btn-primary';
        cardDiv.className = 'card';
        cardDiv.id = `${product.ProductId}`; // set cardDiv->Id to productId from fetched data to use it further
        cartI.className = 'fas fa-cart-plus';
        cardBody.className = 'card-body';
        cardComponentsDiv1.className = 'inline';
        cardComponentsDiv2.className = 'inline';
        cardComponentsDiv2.id = 'cart';
        img.className = 'card-img';
        img.id = 'card-img-id'
        elUrl.className = 'card-url';
        cardDeckDiv.appendChild(cardDiv); // append cardDiv to cardDeckDiv -> "cards container"
        img.setAttribute('src', product.ProductPicUrl);
        H5ProductName.innerHTML = "name: " + product.Name; // get product name from fetched data to product name in card
        pProductPrice.innerHTML = "price: " + product.Price;  // get product price from fetched
        elUrl.appendChild(img);
        elUrl.href = '#';
        cardComponentsDiv1.appendChild(pProductPrice);
        cardComponentsDiv2.appendChild(cartI);
        cardBody.appendChild(cardComponentsDiv1);
        cardBody.appendChild(cardComponentsDiv2);
        cardDiv.appendChild(H5ProductName);
        cardDiv.appendChild(elUrl);
        cardDiv.appendChild(cardBody);
    });
    return products;
})
    // to add array of items added to cart to local storage
    // add sum of products prices to local storage
    // add count of the items in the cart to local storage
    .then(products => {
        // product constructor to select needed data to be added to cartArray 
        let productsString = JSON.stringify(products);
        localStorage.setItem('allItems', productsString);
        var productConstructor = function (productId, productName, price, imgUrl, quantity,existQuantity, status, category, description) {
            this.productId = productId;
            this.productName = productName;
            this.price = price;
            this.imgUrl = imgUrl;
            this.quantity = quantity;
            this.existQuantity = existQuantity;
            this.status = status;
            this.category = category;
            this.description = description;
        };
        var Notexist = true; // flag if the item exits in cartArray or no
        //console.log(products);
        // event to add that item with specific data as object in cartArray
        // save new array to localStorage
        // count all items added to cart and save it to localStorage
        // sum of all products have been added to cartArray and add it to localStorage
        var addCart = function (productid, productArray, selectedQuantity) {
            for (i = 0; i < productArray.length; i++) {
                // check if cardDivId "which is the same as fetched productId" == productId in the fetched array of objects
                if (productArray[i].ProductId == productid) {
                    // create new product object 
                    cartProduct = new productConstructor(productid, productArray[i].Name, productArray[i].Price, productArray[i].ProductPicUrl, selectedQuantity,productArray[i].Quantity, productArray[i].Status, productArray[i].Category, productArray[i].Description);
                   
                    if(selectedQuantity > cartProduct.existQuantity ){
                        break;
                    }                           
                    if (cartArray.length == 0) {
                        // if cartArray is Empty will push product object
                        cartArray = [];
                        if(selectedQuantity > 0 && selectedQuantity <= productArray[i].Quantity){
                            console.log("Empty push");
                            cartProduct.existQuantity -= selectedQuantity;
                            cartArray.push(cartProduct);
                        }
                        Notexist = false;
                    } else {
                        // check if it already exists
                        for (k = 0; k < cartArray.length; k++) {
                            Notexist = true;
                            if (cartArray[k].productId == cartProduct.productId && selectedQuantity <= productArray[i].Quantity) {
                                // if it exists set Notexist flag to false
                                Notexist = false;
                                if (selectedQuantity == 1) {
                                    if(cartArray[k].existQuantity ==0){
                                        alert("Out Of Stock");
                                        break;
                                    }
                                    cartArray[k].quantity++;
                                    cartArray[k].existQuantity--;
                                }else if(selectedQuantity ==0){
                                    cartArray.splice(k,1);
                                }else {
                                    cartArray[k].quantity = selectedQuantity;
                                    cartArray[k].existQuantity = cartProduct.existQuantity - selectedQuantity;
                                    if(cartArray[k].existQuantity ==0){
                                        alert("Out Of Stock");
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    // mmkn agrb a remove el selectedQuantity>0 lw lsa el moshkla mwgoda fe el 2 if 
                    if (Notexist == true && localStorage.getItem('cartArray') != null&&selectedQuantity>0 && selectedQuantity <= productArray[i].Quantity) {
                        // if Notexist == true means that it's new item to the cartArray
                        // then add the item to cartArray
                        console.log("new push");
                        cartArray.push(cartProduct);
                    } else if (localStorage.getItem('cartArray') == null && selectedQuantity >0&& selectedQuantity > 0 && selectedQuantity <= productArray[i].Quantity) {
                        cartArray = [];
                        console.log("repeated push");
                        cartArray.push(cartProduct);
                    }
                    price = cartProduct.price; // take cardProduct price and add it to variable price
                    break;
                }
            }

            allProductsCount = 0;
            sum = 0;
            for (p = 0; p < cartArray.length; p++) {
                allProductsCount += cartArray[p].quantity;
                sum += (cartArray[p].price) * (cartArray[p].quantity);
                console.log("allproductcount afte count loop -> " + allProductsCount);
                console.log("all product sum for loop" + sum);
            }
            if (localStorage.getItem('allProductsCount')) {
                // takke allProductCount data parse it to int then increment 1 to it then add it again to localStorage
                localStorage.setItem('allProductsCount', `${allProductsCount}`);
            } else {
                allProductsCount = selectedQuantity;
                localStorage.setItem('allProductsCount', `${allProductsCount}`);
            }
            if (localStorage.getItem('sum')) {
                // take sum data parse it to int then add price to the sum them add it again to localStorage
                localStorage.setItem('sum', `${sum}`);
            } else {
                // means sum == 0 then add product price to local storage
                    localStorage.setItem('sum', `${sum}`);
            }
            // check if localStorage has allProductCount key or no
            console.log(cartArray.length);
            cartArrayString = JSON.stringify(cartArray); // parse cartArray to string to be added in localStorage
            localStorage.setItem('cartArray', cartArrayString); // add cartArray to localStorage with the new data 

        }
        var cartIconClick = function (event) {
            if (event.target.className == 'fas fa-cart-plus') {
                productId = event.target.parentNode.parentNode.parentNode.id; //take the id of the  html product cardDiv
                addCart(productId, products, 1);
            }
        }
        document.addEventListener('click', cartIconClick);

        return addCart;
    })
    .then(addCart => {
        // create sticky bar with allProductCount , Price of all products in cart , checkOut i from FontAwesome
        var sticky = document.querySelector('.sticky'); // select div from Html file
        var stickyWrapper = document.createElement('div'); // create sticky items wrapper
        var productsCounter = document.createElement('span'); // span for productsCounter
        var productsPrice = document.createElement('span'); // span for productsPrice
        var shoppingCart = document.createElement('i'); // i for checOut i from FontAwesome
        stickyWrapper.className = 'sticky-wrapper';
        productsCounter.id = 'products-counter';
        productsPrice.id = 'products-price';
        shoppingCart.className = 'fas fa-shopping-cart';
        shoppingCart.id = 'fa-shopping-cart';
        stickyWrapper.appendChild(productsCounter);
        stickyWrapper.appendChild(productsPrice);
        stickyWrapper.appendChild(shoppingCart);
        sticky.appendChild(stickyWrapper);
        var counter = localStorage.getItem('allProductsCount'); // take allProductsCount data form localStorage
        // if counter == null then hide the sticky bar
        if (counter == null) {
            document.querySelector('.sticky').style.display = 'none';
        }
        // add sum data and allProductsCount data from localStorage and to handle refresh 
        var sum = localStorage.getItem('sum');
        sum = parseInt(sum);
        counter = parseInt(counter);
        document.querySelector('#products-counter').innerHTML = `${counter}`;
        document.querySelector('#products-price').innerHTML = `$ ${sum}`;
        document.querySelector('#fa-shopping-cart').innerHTML = 'CheckOut';

        return addCart;

    })
    .then(addCart => {
        // update sticky bar data if cartI has been clicked
        var stickyClickCart = function (event) {
            if (event.target.className == 'fas fa-cart-plus') {
                var counter = localStorage.getItem('allProductsCount');
                if (counter != null ) {
                    document.querySelector('.sticky').style.display = 'block';
                }
                var sum = localStorage.getItem('sum');
                sum = parseInt(sum);
                counter = parseInt(counter);
                document.querySelector('#products-counter').innerHTML = `${counter}`;
                document.querySelector('#products-price').innerHTML = `$ ${sum}`;
                document.querySelector('#fa-shopping-cart').innerHTML = 'CheckOut';
            }
        }
        document.addEventListener('click', stickyClickCart);
        return addCart;
    })
    .then(addCart => {
       // console.log(addCart);
        if (localStorage.getItem('allItems')) {
            var allProductsArray = localStorage.getItem('allItems');
            allProductsArray = JSON.parse(allProductsArray);
           //console.log(allProductsArray)
        }

        var productSinglePage = function (event) {
            if (event.target.id == 'card-img-id') {
                document.querySelector('.sticky').style.display = 'none';
                document.querySelector('.card-deck').style.display = 'none';
                var parentElement = document.querySelector('.top-bar');
                while (parentElement.hasChildNodes()) {
                    parentElement.removeChild(parentElement.firstChild);
                }
                productid = event.target.parentNode.parentNode.id;
                var singleItemHtml = '<div class="row" id="single-page"><div class="medium-6 columns"><img class="thumbnail" src="%Img%"></div><div class="medium-6 large-5 columns"><h3>%ProductName%</h3><p>%ProductDescription%</p><div class="row"><div class="small-3 columns"><label for="middle-label" class="middle">Quantity</label></div><div class="small-9 columns"><input type="number" id="middle-label" min="0" max="%maxQuantity%" value="%ProductQuantity%"></div></div><a href="#" class="button large expanded" id="single-cart-btn">addToCart</a></div></div>';
                allProductsArray.forEach(element => {
                    if (element.ProductId == productid) {
                        singleItemHtml = singleItemHtml.replace('%Img%', element.ProductPicUrl);
                        singleItemHtml = singleItemHtml.replace('%ProductName%', element.Name);
                        singleItemHtml = singleItemHtml.replace('%ProductDescription%', element.Description);
                        singleItemHtml = singleItemHtml.replace('%ProductQuantity%', 0);
                        singleItemHtml = singleItemHtml.replace('%maxQuantity%',element.Quantity);
                        console.log(element.Quantity);
                    }
                });
                document.querySelector('.top-bar').insertAdjacentHTML('beforeend', singleItemHtml);
                cartArray.forEach(element => {
                    if (element.productId == productid) {
                        document.getElementById("middle-label").value = element.quantity;
                    }
                });
                document.querySelector('.top-bar').style.display = 'block';

            }
        }
        var addToCartBtn = function (event) {
            if (event.target.id == 'single-cart-btn') {
                var productQuantityValue = parseInt(document.querySelector('#middle-label').value);
                var productStockquantity = parseInt(document.querySelector('#middle-label').max);
                console.log(productStockquantity);
               // console.log(typeof (productQuantityValue));
                /*
                if (productQuantityValue <= 0) {
                    return;
                }*/
                if(productQuantityValue <= productStockquantity){
                    addCart(productid, allProductsArray, productQuantityValue);
                }else{
                    alert("avaliable Quantity is: "+productStockquantity);
                }
                cartArray.forEach(element => {
                    if (element.productId == productid) {
                        document.querySelector('#middle-label').value = element.quantity;
                        document.querySelector('#products-counter').innerHTML = localStorage.getItem('allProductsCount');
                        document.querySelector('#products-price').innerHTML = localStorage.getItem('sum');
                    }
                });
                cartArrayString = JSON.stringify(cartArray);
                localStorage.setItem('cartArray', cartArrayString);
            }
        }
        document.querySelector('#single-cart-btn');
        document.addEventListener('click', productSinglePage);
        document.addEventListener('click', addToCartBtn);


    })
    .then(() => {
        // hide all except shopping-cart html
        var checkOutEvent = function (event) {
            if (event.target.id == 'fa-shopping-cart') {
                document.querySelector('.sticky').style.display = 'none';
                document.querySelector('.card-deck').style.display = 'none';
                var parentElement = document.querySelector('.shopping-cart-tbody');
                while (parentElement.hasChildNodes()) {
                    parentElement.removeChild(parentElement.firstChild);
                }
                var CartItem = '<tr><td class="col-sm-8 col-md-6"><div class="media"><a class="thumbnail pull-left" href="#"> <img class="media-object" src="%img%" style="margin-right: 20px; width: 72px; height: 72px;"> </a><div class="media-body"><h4 class="media-heading"><a href="#">%ProductName%</a></h4><span>Status: </span><span class="text-success"><strong>%Status%</strong></span></div></div></td><td class="col-sm-1 col-md-1" style="text-align: center"><input type="number" class="form-control" value="%ProductQuantity%" disabled></td><td class="col-sm-1 col-md-1 text-center"><strong>%ProductPrice%</strong></td><td class="col-sm-1 col-md-1 text-center"><strong>%PriceXQuantity%</strong></td><td></td></tr>';
                var newCartItem;
                cartArray.forEach(element => {
                    newCartItem = CartItem.replace('%img%', element.imgUrl);
                    newCartItem = newCartItem.replace('%ProductName%', element.productName);
                    if(element.existQuantity == 0){
                        newCartItem = newCartItem.replace('%Status%', "Out Of Stock");
                    }
                    newCartItem = newCartItem.replace('%Status%', element.status);
                    newCartItem = newCartItem.replace('%ProductQuantity%', element.quantity);
                    newCartItem = newCartItem.replace('%ProductPrice%', element.price);
                    newCartItem = newCartItem.replace('%PriceXQuantity%', (element.price * element.quantity));
                    document.querySelector('.shopping-cart-tbody').insertAdjacentHTML('beforeend', newCartItem);
                });
                var totalPriceHtml = '<tr><td>    </td><td>   </td><td>   </td><td><h3>Total</h3></td><td class="text-right"><h3><strong>%Sum%</strong></h3></td></tr>';
                totalPriceHtml = totalPriceHtml.replace('%Sum%', parseInt(localStorage.getItem('sum')));
                document.querySelector('.shopping-cart-tbody').insertAdjacentHTML('beforeend', totalPriceHtml);
                var shoppingCartBtns = '<tr><td>    </td><td>   </td><td>   </td><td><button type="button" class="btn btn-default" id="continue-shopping"><span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping</button></td><td><button type="button" class="btn btn-success" id="checkout-shopping">Checkout <span class="glyphicon glyphicon-play"></span></button></td></tr>'
                document.querySelector('.shopping-cart-tbody').insertAdjacentHTML('beforeend', shoppingCartBtns);
                document.querySelector('.container').style.display = 'block';
            }
        }
        document.addEventListener('click', checkOutEvent);
    })

var HomeFunction = function (event) {
    if (event.target.id == 'Home' || event.target.id == 'continue-shopping') {
        document.querySelector('.container').style.display = 'none';
        document.querySelector('.top-bar').style.display = 'none';
        document.querySelector('#cv').style.display = 'none';
        document.querySelector('.card-deck').style.display = 'flex';
        if (localStorage.getItem('allProductsCount') != undefined) {
            document.querySelector('.sticky').style.display = 'block';
        }
    }
};

var checkOutBtnFnc = function (event) {
    if (event.target.id == "checkout-shopping") {
        document.querySelector('.container').style.display = 'none';
        document.querySelector('.top-bar').style.display = 'none';
        document.querySelector('#cv').style.display = 'none';
        document.querySelector('.card-deck').style.display = 'flex';
        if (localStorage.getItem('allProductsCount') != undefined) {
            document.querySelector('.sticky').style.display = 'none';
        }
        localStorage.clear();
    }
};

var singlePageProudctFun = function (event) {
    if (event.target.id == 'single-cart-btn') {
        document.querySelector('.container').style.display = 'none';
        document.querySelector('.top-bar').style.display = 'none';
        document.querySelector('.card-deck').style.display = 'flex';
        document.querySelector('.top-bar').style.display = 'none';
        document.querySelector('#cv').style.display = 'none';
        document.querySelector('.form-container').style.display = 'none';
        if (localStorage.getItem('allProductsCount') != undefined && localStorage.getItem('allProductsCount') != 0) {
            document.querySelector('.sticky').style.display = 'block';
        }
    }
}

var cvPageFunction = function (event) {
    if (event.target.id == 'About') {
        while(cv.firstChild){
            cv.removeChild(cv.firstChild);
        }
        document.querySelector('#cv').insertAdjacentHTML('beforeend', cvPage);
        document.querySelector('.container').style.display = 'none';
        document.querySelector('.top-bar').style.display = 'none';
        document.querySelector('.card-deck').style.display = 'none';
        document.querySelector('#cv').style.display = 'block';
        document.querySelector('.form-container').style.display = 'none';
        if (localStorage.getItem('allProductsCount') != undefined) {
            document.querySelector('.sticky').style.display = 'none';
        }
    }
}

var contactUsFunction = function(event){
    if(event.target.id == 'Contact-Us' ){
        while(contactUs.firstChild){
            contactUs.removeChild(contactUs.firstChild);
        }
        document.querySelector('.form').insertAdjacentHTML('beforeend', contactUsHtml);
        document.querySelector('.container').style.display = 'none';
        document.querySelector('.top-bar').style.display = 'none';
        document.querySelector('.card-deck').style.display = 'none';
        document.querySelector('#cv').style.display = 'none';
        document.querySelector('.form-container').style.display = 'block';
        if (localStorage.getItem('allProductsCount') != undefined) {
            document.querySelector('.sticky').style.display = 'none';
        }

    }
}

var contact = document.querySelector('.form');
console.log(contact);
contact.addEventListener('submit',function(event) {
    
    
    event.preventDefault();
    var data = {
        name: document.querySelector('#name').value,
        email: document.querySelector('#mail').value,
        subject: document.querySelector('#subj').value,
        message: document.querySelector('#msg').value
    }
    var request = new XMLHttpRequest();
    request.open('post','https://js.vacsera.com/api/final-project?fbclid=IwAR1Ft7dUcGPDGBa1q0A_c4HZqSFwCiPKUuMhGx7oEKLtRhbCATpylHrFmpk');
    request.setRequestHeader('Content-type','application/json');
    request.send(JSON.stringify(data));
    console.log(data);
    
});



document.addEventListener('click', HomeFunction);
document.addEventListener('click', checkOutBtnFnc);
document.addEventListener('click', singlePageProudctFun);
document.addEventListener('click', cvPageFunction);
document.addEventListener('click',contactUsFunction);

/****************************CV Template********************************************/
var cv = document.querySelector('#cv');
var cvPage = '<div class="mainDetails">' +
    '            <div id="headshot" class="quickFade">' +
    '            </div>' +
    '            ' +
    '            <div id="name">' +
    '                <h1 class="quickFade delayTwo">C.V</h1>' +
    '                <h2 class="quickFade delayThree">Engineer</h2>' +
    '            </div>' +
    '            <div id="contactDetails" class="quickFade delayFour">' +
    '                <ul>' +
    '                    <li>e: <a href="https://www.linkedin.com/in/ahmedibrahim94/" target="_blank">LinkedIn</a></li>' +
    '                    <li>m: 01111861133</li>' +
    '                </ul>' +
    '            </div>' +
    '            <div class="clear"></div>' +
    '        </div>' +
    '        ' +
    '        <div id="mainArea" class="quickFade delayFive">' +
    '    ' +
    '            <section>' +
    '                <div class="sectionTitle">' +
    '                    <h1>Work Experience</h1>' +
    '                </div>' +
    '                ' +
    '                <div class="sectionContent">' +
    '                    <article>' +
    '                        <h2>Automation Engineer at AL EZZ DEKHILA STEEL Co.</h2>' +
    '                        <p class="subDetails">April 2018 - June 2018</p>' +
    '                        <p>Level Two Automation Section FlatFactory</p>' +
    '                    </article>' +
    '                    <article>' +
    '                        <h2>Summer Trainee at SENNEBOGEN Maschinenfabrik GmbH</h2>' +
    '                        <p class="subDetails">Jun 2013 - July 2013</p>' +
    '                        <p>     - Operation and maintenance of the machine.' +
    '                                - Function of the main components.' +
    '                                - Description of hydraulic-and electric circuit digram.' +
    '                                - Adjusting and checking the hydrulic pressures.' +
    '                                - Assembling of an 5500 crawler crane.' +
    '                                - Repairing of an HPC 40.</p>' +
    '                    </article>' +
    '                    ' +
    '                    <article>' +
    '                        <h2>Summer Trainee at AL EZZ DEKHILA STEEL Co.</h2>' +
    '                        <p class="subDetails">August 2013 - March 2011</p>' +
    '                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim. Vestibulum bibendum mattis dignissim. Proin id sapien quis libero interdum porttitor.</p>' +
    '                    </article>' +
    '                    ' +
    '                    <article>' +
    '                        <h2>Summer Trainee at BIOSTRADA S.R.L.</h2>' +
    '                        <p class="subDetails">July 2016 - August 2016</p>' +
    '                        <p>During my work placement, I was appointed to the design and development department as well as the production area I got into:' +
    '    ' +
    '                                - The main principle of mechanical sweeper "BioStrada" and the BSA sweeper as well as its essential functions.' +
    '                                - Production-process.' +
    '                                - Creation of technical drawings for the front brush of the mechanical sweeper using solidworks software.' +
    '                                - Prepared a 3D models for some of the main vehicle assemblies of the BSA sweepers like:  the central brush, the lateral brush, the suction cart and the suction turbine. Using solidworks.' +
    '                                - Reversing and modeling some of the machine components which haven\'t any engineering drawings.' +
    '                                </p>' +
    '                    </article>' +
    '                </div>' +
    '                <div class="clear"></div>' +
    '            </section>' +
    '            ' +
    '            ' +
    '            <section>' +
    '                <div class="sectionTitle">' +
    '                    <h1>Key Skills</h1>' +
    '                </div>' +
    '                ' +
    '                <div class="sectionContent">' +
    '                    <ul class="keySkills">' +
    '                        <li>Solid Works</li>' +
    '                        <li>PLC</li>' +
    '                        <li>Java</li>' +
    '                        <li>Java Script</li>' +
    '                        <li>SQL</li>' +
    '                    </ul>' +
    '                </div>' +
    '                <div class="clear"></div>' +
    '            </section>' +
    '            ' +
    '            ' +
    '            <section>' +
    '                <div class="sectionTitle">' +
    '                    <h1>Education</h1>' +
    '                </div>' +
    '                ' +
    '                <div class="sectionContent">' +
    '                    <article>' +
    '                        <h2>Faculty Of Engineering/Alexandria University</h2>' +
    '                        <p class="subDetails">Electromechanical Engineering Program</p>' +
    '                    </article>				' +
    '                </div>' +
    '                <div class="clear"></div>' +
    '            </section>' +
    '            ' +
    '        </div>';
    /*************************ContactUs******************************* */
    var contactUs = document.querySelector('.form');
    var contactUsHtml = 
    '<label for="name">Name</label>'+
'          <input type="text" id="name" name="firstname">'+
'          <label for="mail">Email</label>'+
'          <input type="text" id="mail" name="email">'+
'          <label for="subj">Subject</label>'+
'          <input type="text" id="subj" name="subject">'+
'          <label for="msg">Message</label>'+
'          <input type="text" id="msg" name="message">'+
'          <input type="submit" value="Submit">';
	
        
    